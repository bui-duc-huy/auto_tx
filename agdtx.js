const { Api, JsonRpc } = require("eosjs");
const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig"); // development only
const fetch = require("node-fetch"); //node only
const { TextDecoder, TextEncoder } = require("util"); //node only
const { privateKeys, blockchainLink } = require("./env");

const signatureProvider = new JsSignatureProvider(privateKeys);
const rpc = new JsonRpc(blockchainLink, { fetch }); //required to read blockchain state
const api = new Api({
  rpc,
  signatureProvider,
  textDecoder: new TextDecoder(),
  textEncoder: new TextEncoder(),
}); //required to submit transactions

async function get_table(code, scope, table, index) {
  const result = await rpc.get_table_rows({
    json: true,
    code,
    scope,
    table,
    lower_bound: index,
    limit: 1,
    reverse: false,
    show_payer: false,
  });

  console.log(result);

  return result;
}

// get_table("agd.tokent", "agd.tokent", "currency", 13);

async function new_type(token_name) {
  const result = await api.transact(
    {
      actions: [
        {
          account: "dgoodst",
          name: "create",
          authorization: [
            {
              actor: "dgoodst",
              permission: "active",
            },
          ],
          data: {
            issuer: "agd.tokent",
            rev_partner: "agd.tokent",
            category: "agd.area",
            token_name,
            fungible: true,
            burnable: true,
            sellable: true,
            transferable: true,
            rev_split: 0,
            base_uri: "agridential.vn",
            max_issue_days: 0,
            max_supply: "1000000000000000000 GOOD",
          },
        },
        {
          account: "dgoodst",
          name: "issue",
          authorization: [
            {
              actor: "agd.tokent",
              permission: "active",
            },
          ],
          data: {
            to: "agd.tokent",
            category: "agd.area",
            token_name,
            quantity: "1000000000000000000 GOOD",
            relative_uri: "agridential.vn",
            memo: "issue token using in agridential system",
          },
        },
        {
          account: "dgoodst",
          name: "create",
          authorization: [
            {
              actor: "dgoodst",
              permission: "active",
            },
          ],
          data: {
            issuer: "agd.tokent",
            rev_partner: "agd.tokent",
            category: "agd.box",
            token_name,
            fungible: true,
            burnable: true,
            sellable: true,
            transferable: true,
            rev_split: 0,
            base_uri: "agridential.vn",
            max_issue_days: 0,
            max_supply: "1000000000000000000 GOOD",
          },
        },
        {
          account: "dgoodst",
          name: "issue",
          authorization: [
            {
              actor: "agd.tokent",
              permission: "active",
            },
          ],
          data: {
            to: "agd.tokent",
            category: "agd.box",
            token_name,
            quantity: "1000000000000000000 GOOD",
            relative_uri: "agridential.vn",
            memo: "issue token using in agridential system",
          },
        },
        {
          account: "dgoodst",
          name: "create",
          authorization: [
            {
              actor: "dgoodst",
              permission: "active",
            },
          ],
          data: {
            issuer: "agd.tokent",
            rev_partner: "agd.tokent",
            category: "agd.item",
            token_name,
            fungible: true,
            burnable: true,
            sellable: true,
            transferable: true,
            rev_split: 0,
            base_uri: "agridential.vn",
            max_issue_days: 0,
            max_supply: "1000000000000000000 GOOD",
          },
        },
        {
          account: "dgoodst",
          name: "issue",
          authorization: [
            {
              actor: "agd.tokent",
              permission: "active",
            },
          ],
          data: {
            to: "agd.tokent",
            category: "agd.item",
            token_name,
            quantity: "1000000000000000000 GOOD",
            relative_uri: "agridential.vn",
            memo: "issue token using in agridential system",
          },
        },
      ],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

async function new_token(customer, id_item, category, token_name, quantity) {
  const result = await api.transact(
    {
      actions: [
        {
          account: "dgoodst",
          name: "create",
          authorization: [
            {
              actor: "dgoodst",
              permission: "active",
            },
          ],
          data: {
            issuer: "agd.tokent",
            rev_partner: "agd.tokent",
            category: "agd.area",
            token_name,
            fungible: true,
            burnable: true,
            sellable: true,
            transferable: true,
            rev_split: 0,
            base_uri: "agridential.vn",
            max_issue_days: 0,
            max_supply: "1000000000000000000 GOOD",
          },
        },
        {
          account: "dgoodst",
          name: "issue",
          authorization: [
            {
              actor: "agd.tokent",
              permission: "active",
            },
          ],
          data: {
            to: "agd.tokent",
            category: "agd.area",
            token_name,
            quantity: "1000000000000000000 GOOD",
            relative_uri: "agridential.vn",
            memo: "issue token using in agridential system",
          },
        },
        {
          account: "dgoodst",
          name: "create",
          authorization: [
            {
              actor: "dgoodst",
              permission: "active",
            },
          ],
          data: {
            issuer: "agd.tokent",
            rev_partner: "agd.tokent",
            category: "agd.box",
            token_name,
            fungible: true,
            burnable: true,
            sellable: true,
            transferable: true,
            rev_split: 0,
            base_uri: "agridential.vn",
            max_issue_days: 0,
            max_supply: "1000000000000000000 GOOD",
          },
        },
        {
          account: "dgoodst",
          name: "issue",
          authorization: [
            {
              actor: "agd.tokent",
              permission: "active",
            },
          ],
          data: {
            to: "agd.tokent",
            category: "agd.box",
            token_name,
            quantity: "1000000000000000000 GOOD",
            relative_uri: "agridential.vn",
            memo: "issue token using in agridential system",
          },
        },
        {
          account: "dgoodst",
          name: "create",
          authorization: [
            {
              actor: "dgoodst",
              permission: "active",
            },
          ],
          data: {
            issuer: "agd.tokent",
            rev_partner: "agd.tokent",
            category: "agd.item",
            token_name,
            fungible: true,
            burnable: true,
            sellable: true,
            transferable: true,
            rev_split: 0,
            base_uri: "agridential.vn",
            max_issue_days: 0,
            max_supply: "1000000000000000000 GOOD",
          },
        },
        {
          account: "dgoodst",
          name: "issue",
          authorization: [
            {
              actor: "agd.tokent",
              permission: "active",
            },
          ],
          data: {
            to: "agd.tokent",
            category: "agd.item",
            token_name,
            quantity: "1000000000000000000 GOOD",
            relative_uri: "agridential.vn",
            memo: "issue token using in agridential system",
          },
        },
        {
          account: "agd.tokent",
          name: "issue",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            owner: customer,
            id_item: id_item,
            category,
            token_name,
            quantity,
            memo: "issue token using in agridential system",
          },
        },
        {
          account: "agd.treet",
          name: "newtree",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            id_first_node: id_item,
            id_tree: id_item,
            description: `tree for tracking item ${id_item}`,
          },
        },
      ],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

async function divide(
  customer,
  id_item,
  parts,
  quantity_per_part,
  new_items_id,
  memo
) {
  const result = await api.transact(
    {
      actions: [
        {
          account: "agd.tokent",
          name: "divideft",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            id_item,
            parts,
            quantity_per_part,
            new_items_id,
            memo,
          },
        },
        {
          account: "agd.treet",
          name: "newnode",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            id_new_nodes: new_items_id,
            id_parent_nodes: [id_item],
          },
        },
      ],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

async function transfer(customer, id_item, to, memo) {
  const actions = [
    {
      account: "agd.tokent",
      name: "transferitem",
      authorization: [
        {
          actor: customer,
          permission: "active",
        },
      ],
      data: {
        from: customer,
        to,
        id_item,
        memo,
      },
    },
  ];

  const table = await get_table(
    "agd.tokent",
    "agd.tokent",
    "currency",
    id_item
  );

  actions.push({
    account: "eosio.tokent",
    name: "transfer",
    authorization: [
      {
        actor: customer,
        permission: "active",
      },
    ],
    data: {
      from: customer,
      to,
      quantity: table.rows[0].balance,
      memo,
    },
  });

  actions.push({
    account: "dgoodst",
    name: "transferft",
    authorization: [
      {
        actor: customer,
        permission: "active",
      },
    ],
    data: {
      from: customer,
      to,
      category: table.rows[0].category,
      token_name: table.rows[0].token_name,
      quantity: "1 GOOD",
      memo,
    },
  });

  const result = await api.transact(
    {
      actions: actions,
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

async function change(
  customer,
  id_item,
  new_id_item,
  category,
  token_name,
  memo
) {
  const table = await get_table(
    "agd.tokent",
    "agd.tokent",
    "currency",
    id_item
  );
  const result = await api.transact(
    {
      actions: [
        {
          account: "agd.tokent",
          name: "changetoken",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            owner: customer,
            id_item,
            new_id_item,
            category,
            token_name,
            memo,
          },
        },
        {
          account: "agd.treet",
          name: "newnode",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            id_new_nodes: [new_id_item],
            id_parent_nodes: [id_item],
          },
        },
        {
          account: "dgoodst",
          name: "transferft",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            from: customer,
            to: "agd.tokent",
            category: "agd.area",
            token_name: token_name,
            quantity: `1 GOOD`,
            memo,
          },
        },
        {
          account: "eosio.tokent",
          name: "transfer",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            from: customer,
            to,
            quantity: table.rows[0].balance,
            memo,
          },
        },
      ],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

async function merge(
  customer,
  id_items,
  new_id_item,
  category,
  token_name,
  memo
) {
  const result = await api.transact(
    {
      actions: [
        {
          account: "agd.tokent",
          name: "mergeitem",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            owner: customer,
            id_items,
            new_id_item,
            category,
            token_name,
            memo,
          },
        },
        {
          account: "agd.treet",
          name: "newnode",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            id_new_nodes: [new_id_item],
            id_parent_nodes: id_items,
          },
        },
        {
          account: "dgoodst",
          name: "transferft",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            from: customer,
            to: "agd.tokent",
            category: "agd.area",
            token_name: token_name,
            quantity: `${id_items.length - 1} GOOD`,
            memo,
          },
        },
      ],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

async function add_daily_action(
  customer,
  id_item,
  action,
  from,
  to,
  description,
  date
) {
  const result = await api.transact(
    {
      actions: [
        {
          account: "agd.ledgert",
          name: "adddiary",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            id_item,
            action,
            from,
            to,
            description,
            date,
          },
        },
      ],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

async function set_metadata(
  customer,
  id_item,
  name_item,
  stay,
  dependency,
  supply
) {
  const result = await api.transact(
    {
      actions: [
        {
          account: "agd.ledgert",
          name: "setmetadata",
          authorization: [
            {
              actor: customer,
              permission: "active",
            },
          ],
          data: {
            id_item,
            name_item,
            stay,
            dependency,
            supply,
          },
        },
      ],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );

  console.log(result);
}

// transfer();

// new_token("duchuy", 24, "agd.area", "to.yen1", "1.0000 AREA");
// set_metadata(
//   "duchuy",
//   24,
//   "to yen phu quoc",
//   "to yen 1",
//   "phu quoc",
//   "1000 to yen"
// );

// add_daily_action(
//   "duchuy",
//   24,
//   "nuoi yen",
//   "cong ty nuoi yen Phu Quoc",
//   "To yen 24",
//   "bat dau nuoi yen",
//   "28/01/2021"
// );

change();

// merge(
//   "customer",
//   [19],
//   20,
//   "agd.box",
//   "to.yen",
//   "thu hoach to yen thanh cac thung yen"
// );

// divide(
//   "customer",
//   20,
//   3,
//   ["1.0000 BOX", "1.0000 BOX", "1.0000 BOX"],
//   [21, 22, 23],
//   "tach thung yen thanh cac thung nho hon"
// );

// add_daily_action(
//   "customer",
//   19,
//   "nuoi yen",
//   "cong ty nuoi yen Phu Quoc",
//   "To yen 19",
//   "tach thung yen thanh cac thung nho hon",
//   "28/01/2021"
// );

// transfer("customer", 23, "eosio", "ban to yen");

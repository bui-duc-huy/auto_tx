const { Api, JsonRpc } = require("eosjs");
const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig"); // development only
const fetch = require("node-fetch"); //node only
const { TextDecoder, TextEncoder } = require("util"); //node only
const { privateKeys, blockchainLink } = require("./env");

const signatureProvider = new JsSignatureProvider(privateKeys);
const rpc = new JsonRpc(blockchainLink, { fetch }); //required to read blockchain state
const api = new Api({
  rpc,
  signatureProvider,
  textDecoder: new TextDecoder(),
  textEncoder: new TextEncoder(),
}); //required to submit transactions

function randomNumber() {
  return Math.floor(Math.random() * Math.floor(3));
}

function randomString(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function randomAction() {
    const action_index = randomNumber()
    if (action_index == 0) {
        return  {
            account: "recor",
            name: "setvalue",
            authorization: [
                {
                    actor: "duchuy",
                    permission: "active",
                },
            ],
            data: {
                user: "duchuy",
                new_value: randomNumber(),
            },
        }
    } else {
        return actions[action_index]
    }
}

const actions = [
    {
    account: "recor",
    name: "getvalue",
    authorization: [
      {
        actor: "duchuy",
        permission: "active",
      },
    ],
    data: {
      user: "eosio",
    },
  },
  {
    account: "recor",
    name: "hi",
    authorization: [
      {
        actor: "duchuy",
        permission: "active",
      },
    ],
    data: {
      user: "eosio",
    },
  },
];

let count = 0

async function pushAction() {
  const result = await api.transact(
    {
        actions: [randomAction()],
    },
    {
      blocksBehind: 10,
      expireSeconds: 50,
    }
  );
    count++
    console.log("transaction sent", count)
    console.log(result)
}

async function getBlock(blockNumber) {
  const block = await rpc.get_block(blockNumber);
  console.log(block.transactions[0].trx.transaction);
}

async function getTransaction() {
  const transaction = await rpc.history_get_transaction(
    "f1d648cd811945e63a58e89507ff6ac5197937aef0d24c72577daf28be0b54db",
    530915
  );
  console.log(transaction.trx.trx.actions);
}
setInterval(() => {
  pushAction();
}, 7200000);

pushAction()

// getTransaction()
// getBlock(531646)

// func()
